package com.beauty.framework.workflow.mappers;

public interface BeautyWorkFlowMybatisMapper {
    String loadProcessDefinitionIdByKey(String key);
}
